package app.utils;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JsonUtils {

    public static <T> List<T> fromJsonToList(String json, Class<T> clazz) throws IOException {
        TypeFactory typeFactory = TypeFactory.defaultInstance();
        JavaType javaType = typeFactory.constructCollectionType(LinkedList.class, clazz);
        return new ObjectMapper().readValue(json, javaType);
    }
}
