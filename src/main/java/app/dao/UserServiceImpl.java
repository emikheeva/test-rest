package app.dao;

import app.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private static String resourceFileName = "users.txt";

    private final List<User> userList = new ArrayList<>();

    @Override
    public List<User> getUsersByMonth (Integer month){
        Calendar calendar = getMidnight();
        long date = calendar.getTimeInMillis();

        if (month == null)
            month = calendar.get(Calendar.MONTH);
        else
            month--;

        int mon = month.intValue();
        return userList.stream().filter(u -> u.getBirthdate().getMonth() == mon)
                .map(u -> {
                    Calendar birthdate = Calendar.getInstance();
                    birthdate.setTime(u.getBirthdate());
                    birthdate.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
                    if (birthdate.before(calendar))
                        birthdate.add(Calendar.YEAR, 1);

                    Long daysLast = (birthdate.getTimeInMillis() - date) / (24*60*60*1000);
                    u.setDaysLeft(daysLast.intValue());
                    return u;
                })
                .collect(Collectors.toList());
    }

    private Calendar getMidnight(){
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    @PostConstruct
    private void init(){
        File file = new File(getClass().getClassLoader().getResource(resourceFileName).getFile());
        if (file != null) {
            try (BufferedReader br = new BufferedReader(new FileReader(file));) {
                String line = null;
                StringBuilder resultBuilder = new StringBuilder();

                while ((line = br.readLine()) != null) {
                    resultBuilder.append(line);
                }

                String result = resultBuilder.toString();
                if (result != null && !result.isEmpty()) {
                    List<User> users = JsonUtils.fromJsonToList(result, User.class);
                    userList.addAll(users);
                    logger.info("List of users initialized successfully.");
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
        else
            logger.error(String.format("List of users initialization failed. " +
                    "Resource file \"%s\" does not exist.", resourceFileName));
    }


}
