package app.dao;

import app.utils.CustomJsonDateDeserializer;
import app.utils.CustomJsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

public class User {

    private String firstName;
    private String lastName;
    @JsonSerialize(using = CustomJsonDateSerializer.class)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class, as = Date.class)
    private Date birthdate;
    private Integer daysLeft;

    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public Date getBirthdate(){
        return birthdate;
    }
    public void setBirthdate(Date birthdate){
        this.birthdate = birthdate;
    }

    public Integer getDaysLeft(){
        return daysLeft;
    }
    public void setDaysLeft(Integer daysLeft){
        this.daysLeft = daysLeft;
    }
}
