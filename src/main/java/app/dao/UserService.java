package app.dao;

import java.util.List;

public interface UserService {

    /**
     * Returns list of users with a birthday in a definite month.
     * @param month
     * @return
     */
    public List<User> getUsersByMonth (Integer month);
}
