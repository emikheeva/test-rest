package app.api;

import app.exception.ValidationException;
import app.logic.FindUserService;
import app.dao.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BirthdayController {
    private static Logger logger = LoggerFactory.getLogger(BirthdayController.class);

    @Autowired
    FindUserService findUserService;

    @RequestMapping(value = "/getUsersFindTask/", method = RequestMethod.POST)
    public
    @ResponseBody
    Integer postFindTask(
            @RequestParam(value = "month", required = false) Integer month)
    throws ValidationException{
        return findUserService.initFindUserTask(month);
    }

    @RequestMapping(value = "/getUsersFindTask/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Object> getFindTask(@PathVariable Integer id) throws ValidationException{
        List<User> result = findUserService.getFindUserTaskResult(id);
        if (result != null)
            return new ResponseEntity<>(result, HttpStatus.OK);
        else
            return new ResponseEntity<>(String.format("\"Task #%d is processing.\"", id), HttpStatus.ACCEPTED);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = ValidationException.class)
    @ResponseBody
    public String handle(ValidationException ex) {
        logger.error(ex.getMessage(), ex);
        return String.format("\"%s\"", ex.getMessage());
    }
}
