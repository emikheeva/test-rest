package app.logic;

import app.dao.User;
import app.dao.UserService;
import app.exception.ValidationException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class FindUserServiceImpl implements FindUserService {
    private static Logger logger = LoggerFactory.getLogger(FindUserServiceImpl.class);

    @Autowired
    UserService userService;

    private ConcurrentHashMap<Integer, FutureTask<List<User>>> taskMap = new ConcurrentHashMap<>();
    private AtomicInteger ids = new AtomicInteger(0);
    Executor executor = Executors.newCachedThreadPool();

    @Override
    public Integer initFindUserTask(Integer month) throws ValidationException{
        if (month != null && (month < 1 || month > 12))
            throw new ValidationException("Parameter [month] is not valid. Value should in [1;12].");

        int id = ids.incrementAndGet();
        FindUserCallable task = new FindUserCallable(id, month, userService);
        FutureTask<List<User>> fTask = new FutureTask<List<User>>(task);
        taskMap.put(id, fTask);
        executor.execute(fTask);
        return id;
    }

    @Override
    public List<User> getFindUserTaskResult(int taskId) throws ValidationException {
        FutureTask<List<User>> fTask = taskMap.get(taskId);
        if (fTask != null){
            if (fTask.isDone())
                try {
                    return fTask.get();
                }
                catch (InterruptedException | ExecutionException ex){
                    logger.error(ex.getMessage(), ex);
                }
            else
                return null;
        }
        throw new ValidationException(String.format("Task #%d does not exist.", taskId));
    }
}
