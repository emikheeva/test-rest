package app.logic;

import app.dao.User;
import app.dao.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Callable;


public class FindUserCallable implements Callable<List<User>> {
    private static Logger logger = LoggerFactory.getLogger(FindUserCallable.class);

    private final int id;
    private final Integer month;
    UserService userService;

    public FindUserCallable(int id, Integer month, UserService userService){
        this.id = id;
        this.month = month;
        this.userService = userService;
        logger.info(String.format("Task #%d created %s", id,
                month == null ? ". Month is not specified." : String.format("for month = %d.", month)));
    }

    public List<User> call() throws InterruptedException{
        logger.info(String.format("Task #%d started execution.", id));
        Thread.sleep(60000);
        List<User> result = userService.getUsersByMonth(month);
        logger.info(String.format("Task #%d finished execution. %d users were found.", id, result.size()));
        return result;
    }
}
