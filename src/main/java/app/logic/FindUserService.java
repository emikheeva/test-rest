package app.logic;

import app.dao.User;
import app.exception.ValidationException;

import java.util.List;

public interface FindUserService {

    /**
     * Creates new task searching for users with a birthday in definite month.
     * Returns an identifier of the task.
     * Throws ValidationException if month is not in [1;12].
     * @param month
     * @return
     * @throws ValidationException
     */
    public Integer initFindUserTask(Integer month) throws ValidationException;

    /**
     * Returns the result of the task with definite identifier (list of users with a birthday in definite month)
     * if it is ready, null - if task is in process.
     * Throws ValidationException if task with definite identifier does not exist.
     * @param taskId
     * @return
     * @throws ValidationException
     */
    public List<User> getFindUserTaskResult(int taskId) throws ValidationException;
}
